package ste.demolibrary;

import demolibrary.library.TestPrinter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemolibraryApplication {

    public static void main(String[] args) {
        TestPrinter printer = new TestPrinter();
        printer.setPrinterName("super test");
        printer.setMessage("sfiusgsigfub");

        SpringApplication.run(DemolibraryApplication.class, args);
        printer.printMessage();
    }

}
