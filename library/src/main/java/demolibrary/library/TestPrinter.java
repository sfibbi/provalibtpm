package demolibrary.library;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

@Service
@EnableConfigurationProperties(ServiceProperties.class)

public class TestPrinter {
    private String message;
    private String printerName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public void printMessage() {
        System.out.println("Messaggio da stampante " + this.printerName + ": " + this.message);
    }
}
